---
title: README
status: hidden
---

# Inhaltsrepo der Fachgruppenwebseite

[![pipeline status](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data/badges/main/pipeline.svg)](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website-data/-/commits/main)
[![meta pipeline status](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website/badges/master/pipeline.svg)](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website/-/commits/master)

Dieses Repo beinhaltet den Inhalt der [Webseite der Fachgruppe Informatik](https://fg.informatik.uni-goettingen.de/).

Die Seitenkonfiguration, das Theme und wie die ganze Seite gebaut wird, findest du im [Metarepo](https://gitlab.gwdg.de/GAUMI-fginfo/fg-website).

## Dokumentation

[**Die Dokumentation zum Erstellen von Inhalten findest du hier.**](https://gaumi-fginfo.pages.gwdg.de/fg-website/docs-output/de/tag/content.html)

## Deployment

Sobald man in einem der beiden Repos Änderungen macht, wird automatisch eine CI/CD Pipeline ausgeführt, welche die neuen Änderungen baut und direkt auf der Webseite veröffentlicht.
Dies kann bis zu 5 Minuten dauern.

Falls die Seite nach dem Durchführen der Pipelines nicht aktuallisiert ist, muss man vielleicht die Seite mit Strg + F5 neuladen, damit der Browser die Änderungen auch übernimmt.

