---
title: "Räumlichkeiten"
color: "hsl(294, 62%, 39%)"
slug: "tag-raeume"
lang: de
after:
  - type: "iframe"
    url: "https://wwwuser.gwdg.de/~fginfo/i/"
    title:
      de: "Virtuelle Institutstour (2020)"

---

<!--Die Räume des Institus sind unergründlich... *oh* hier sind sie ja aufgelistet.
Zumindest alle Räumlichkeiten die für dein Studium relevant sind.-->

Hier sind alle für dein Studium relevante Räumlichkeiten.

