---
title: "Veranstaltungen"
color: "hsl(345, 86%, 56%)"
slug: "tag-event"
lang: de
---

Die Fachgruppe organisiert regelmäßig Veranstaltungen mit den unterschiedlichsten Inhalten, so dass für alle etwas dabei ist. 
Highlight im Wintersemester ist neben der [O-Phase](slug:ophase) die [Weihnachtsvorlesung](slug:weihnachtsvorlesung) und im Sommersemester die [studentischen Informatiktage](slug:sit).


