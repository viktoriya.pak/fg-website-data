---
title: "Engagier Dich!"
color: "hsl(157, 77%, 37%)"
slug: "tag-engage"
lang: de
---

Ohne eure Unterstützung gäbe es viele Angebote im und ums Studium herum nicht.
Hier findet ihr deshalb ein paar Anregungen, wie ihr euch einbringen könnt, von der Mithilfe in der OPhase bis zum aktiven Mitmachen in der Fachgruppenarbeit, aber auch darüberhinaus.
