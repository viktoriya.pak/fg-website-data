---
title: "Über uns"
slug: fachgruppe
lang: de
---

Wir, die Fachgruppe Informatik, befassen uns mit Qualität und Inhalt der Informatik Studiengänge in Göttingen. Wir arbeiten an Verbesserungen, um das Studium für alle attraktiver zu gestalten. Das bedeutet, wir studieren selber im Bereich Informatik, sammeln Erfahrungen und Interessen und vertreten diese im Namen der Studierendenschaft. Einige von uns sitzen dafür in den Gremien der Universität. Zusätzlich kümmern wir uns um Veranstaltungen wie die O-Phase, Weihnachtsvorlesungen oder auch die [Cookie-Talks](slug:cookie-talks).
Über eure studentische Emailadresse werdet ihr über Veranstaltungen und andere wichtige Themen auf dem Laufenden gehalten. Außerdem gibt es einen Telegram-Channel auf dem wir euch auf dem Laufenden halten mit einem angegliedertem Diskussionraum der mehr Interaktion als Email ermöglicht (In der App könnt ihr nach infounigoe suchen). 

Jede:r Studierende der:die sich für mehr Abwechselung neben dem Informatikstudium einsetzen möchte, gerne etwas verändern will oder auch nur mal rein schauen möchte, ist herzlich eingeladen, bei unseren [wöchentlichen Treffen](slug:fgtreffen) vorbei zuschauen. Wir treffen uns im Raum 0.101 im IfI in einer offenen Runde jeden Donnerstag um 18Uhr.

Wir sind auch jederzeit per E-Mail erreichbar unter: [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
Aktuell ist Pauline von der Haar unsere Fachgruppensprecherin.

Wir freuen uns auf euch! Eure Fachgruppe Informatik

Einen digitalen Rundgang durch das Institut für Informatik findest du [hier](http://wwwuser.gwdg.de/~fginfo/i/).
