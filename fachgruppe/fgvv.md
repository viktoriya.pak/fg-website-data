---
title: "Fachgruppen&shy;vollversammlung (FGVV)"
slug: fgvv
tags: fachgruppe, event
lang: de
relevant:
  end: 2022-10-29
  prio: 5
---

:::success
__Aktuelle Einladung zur FGVV im Wintersemester 2022/23__

Liebe Informatik-Studierendenschaft,
Wir, die aktive Fachgruppe Informatik, möchten euch am 28.10. um 16:00 Uhr [c.t.] zur Fachgruppenvollversammlung einladen! Hier besprechen wir Themen, die jede\*n Informatikstudi etwas angehen, wie z.B. unseren Finanzplan. Im Anschluss wird es Crepes und, da Freitag ist, einen Spielenabend geben. Die Versammlung wird im MN.. stattfinden.
Die folgenden Punkt stehen auf der Tagesordnung:
1. Begrüßung
2. Organisatorisches und Formalia
3. Bericht der Fachgruppensprecherin
 ... Sommersemester 2022
 ... Planung Wintersemester 2022/23
 ... Finanzübersicht
4. Vorstellung von Angeboten zur Studienqualitätssicherung
 ... QM-Track
 ... Gleichstellungsteam
5. Veranstaltungswünsche und -ideen an die Fachgruppe
6. Vereinbarkeit von Studium und Studienfinanzierung

Die offizielle Einladung findet ihr [ausgehängt im Institut für Informatik].
Ich freue mich auf euch!
Pauline (Fachgruppensprecherin)
und die Fachgruppe Informatik
:::

Eine Fachgruppenvollversammlung ist ein Forum der hochschulpolitischen Diskussion der Fachgruppenarbeit und hat die Funktion, die Kommunikation in der Fachgruppe zu fördern.
Alle Fachgruppenmitglieder, also alle Infostudis, haben Stimmrecht auf der FGVV. Unteranderem die\*der Fachgruppensprecher\*in können eine FGVV einberufen. Eine Einladung muss mindestens 4 Vorlesungstage vorher angekündigt werden. Das und einiges Weiteres zur FGVV steht in der Organisationssatzung der Studierendenschaft (§33 und §34).
