---
title: Startseite
template: "index.html"
after:
  - type: "relevant"
    title:
      de: "Aktuelles"
    id: "relevant"
  - type: "iframe"
    url: "https://cloud.asta.uni-goettingen.de/apps/calendar/embed/g8psWpbfafNM6Rpo"
    title:
      de: "Kalender"
    id: "calendar"
  - type: "tag"
    tag: "engage"
    num: 5
    id: "engage"
  - type: "news"
    title:
      de: "Zuletzt Veränderte Seiten"
    id: "last_changed"
    num: 5

---

Willkommen auf der Webseite der Fachgruppe Informatik! Hier findest du allgemeine Informationen über uns, aktuelle Fachgruppen-Veranstaltungen, dein Studium und vieles mehr.  
