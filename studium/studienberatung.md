---
title: "Studienberatung für Erstsemester in ganz kurz"
slug: studienberatung
lang: de
relevant:
  end: 2022-12-01
  prio: 4

---

[TOC]

## Aufgezeichnete Studienberatung für Angewandte Informatik B.Sc.
Die Aufzeichnung von der Studienberatung für Angewandte Informatik (B.Sc.) vom 17.10.2022 findest du unten auf der folgenden Seite: https://meet.gwdg.de/b/fac-bv0-2ng-uol.

## :flight_departure: Getting Started

:::info
Hier sind die Schritte die du für dein Studium zu Beginn tun solltest.
:::

### :one: Was man im Ersten Semester hören muss/sollte

- Mono Bachelor Angew. Informatik (B.Sc.):
	1. Informatik I / Grundlagen der Informatik und Programmierung (wird gerade umbenannt; Benennung ist deshalb noch nicht einheitlich)
	2. Diskrete Mathematik
	3. Mathematik für Studierende der Informatik I / Mathematik für InformatikanfängerInnen I (oft auch Mafia I genannt)

- 2-Fächer Bachelor Informatik (beide Profile):
	1. Informatik I / Grundlagen der Informatik und Programmierung (wird gerade umbenannt; benennung ist deshalb noch nicht einheitlich)
	2. Diskrete Mathematik

### :two: Sich im Studip dazu eintragen

Im Studip kannst du dich zu den Veranstaltungen anmelden.

Das Studip findest du unter [studip.uni-goettingen.de](https://studip.uni-goettingen.de) oder unter [ecampus.uni-goettingen.de](https://ecampus.uni-goettingen.de).
In den entsprechenden Studip-Veranstaltungen gibt es eine Liste mit Terminen. Der erste davon ist der Termin der ersten Vorlesung.

### :three: Zur ersten Vorlesung gehen bzw. deren digitales Äquivalent

Dort bekommt man alle weiteren Informationen  über:
- weitere Termine (Tutorium/Übungsgruppe)
- Anmeldung zur Übungsgruppe
- Klausurzulassung (i.d.R. 50% der Punkte aus den Übungszetteln/Hausaufgabenzetteln)
- Klausurtermine und Klausur

### :four: Im Flexnow für die Übung in Informatik I anmelden.

Das Flexnow findes du unter [pruefung.uni-goettingen.de](https://pruefung.uni-goettingen.de) oder im [Ecampus](https://ecampus.uni-goettingen.de).

### :five: Erstmal sehen, dass man das 1. Semester meistert

Plane noch nicht gleich dein ganzes Studium.
Im ersten Semester lernst du viel über das Studieren an sich, was du für dein Studium an Veranstaltungen brauchst und wann du diese hören solltest.

---

:::warning
Für den **2-Fächer-Bachelor** kann man von Informatikseite den obrigen Vorschlag verwenden. Andere Fächer haben aber möglicherweise noch strengere Regeln. Guckt deshalb unbedingt in die Studienordnung beider Fächer. Ihr findet sie mit der Suchmaschine eurer Wahl auf den Seiten der Uni Göttingen.
:::

## :arrow_heading_down: Möglichkeiten vom obrigen Plan abzuweichen
Wenn du **Vorkenntnisse** hast (z.B. Informatik LK, Frühstudium) oder schon Module **angerechnet** bekommen kannst, kannst du überlegen mehr zu machen. Emfehlenswert wäre hier zunächst der C-Kurs (Programmierkurs/Grundlagen der C-Programmierung). Ansonsten wende dich an studienberatung@informatik.uni-goettingen.de.

Später im Studium kann man sich in verschiedene Richtungen (Studienschwerpunkte) spezialisieren. Statt _Mathematik für Studierende der Informatik I_ (Mafia I), kannst du auch _Analytische Geometrie und Lineare Algebra I_ (AGLA I) und _Differenzial- und Integralrechnung I_ bzw. Analysis I (DIFF I) hören. Empfehlenswert ist dies für Interessierte an den Schwerpunkten *wiss. Rechnen*, *Neuroinformatik* oder *Bioinformatik*.

Analog gibt es die _Mathematik für Studierende der Physik_ (MaPhy), die sich für diejenigen empfiehlt, die überlegen den neuen Schwerpunkt *Computational Physics* zu besuchen. Die _Mathematik für Studierende der Physik_ deckt zusätzliche Themen ab, ist aber mehr Aufwand was auch bedeutet, dass man ein Informatikmodul weniger hört bzw. hören muss.

:::warning
Das ist im 2-Fächer Bachelor ohne Mathematik __nicht__ möglich! Hier muss eine der Kombinationen __Mafia I, II__, __Diskrete Mathematik, Mafia I__ oder __Diskrete Mathematik__ (einzeln) besucht werden!
:::

## :ear: Davon solltest du hören/gehört haben
- Übungszettel
- Klausurzulassung (verm. 50% der Punkte der Zettel)
- Anmeldung zu den Übungsgruppen
- Anmeldung zur Klausurzulassung im FlexNow
- Abgabetermin der Übungszettel
- u.v.m.

## :checkered_flag: Gegen Ende des ersten Semesters (Mono)
- Für alle Übungen und Klausuren angemeldet sein
- C-Kurs (Programmierkurs) hören
- Die Studienschwerpunktvorstellung in Informatik I besuchen (keine Sorge, das wird noch bekannt gegeben)
- entscheiden, welche Schwerpunkte dich interessieren und welche du dir genauer angucken möchtest
- das zweite Semester planen

## :checkered_flag: Gegen Ende des ersten Semesters (2-Fächer)
- Für alle Übungen und Klausuren angemeldet sein
- für mögliche Sprachkurse anmelden
- entscheiden, welche Fachbereiche dich interessieren und welche du dir genauer angucken möchtest
- das zweite Semester planen

## :question: Was tun bei Fragen und Problemen?
Bei Fragen und Problem ist zunächst die studienberatung@informatik.uni-goettingen.de verantwortlich. Die Studienberater:innen sind sehr nette Menschen die dafür bezahlt werden dir zu helfen.

Alternativ kannst du auch die [Fachgruppe Informatik](mailto:fachgruppe@informatik.uni-goettingen.de) fragen. Besonders freuen wir uns über Anregungen und Probleme mit dem Studienaufbau (wenn z.B. Unmögliches von dir verlangt wird).

## :capital_abcd: Begriffliche Erklärungen

Übungszettel
: Es gibt jede Woche einen. Ihr müsst den machen und abgeben, damit ihr die Klausurzulassung bekommt.

Klausurzulassung
: Wird in der ersten Vorlesung erklärt. Ihr müsst euch dafür im Flexnow anmelden. In Informatik I müsst ihr das ganz früh tun, in Mathe vermutlich etwas später.

Modul / Veranstaltung
: Module sind eine abstrakte Idee davon was und wie man etwas lernt. Sie werden durch Veranstaltungen implementiert (praktisch ausgestaltet). Module können aus mehreren Teilveranstaltungen bestehen. Es kann zu einem Modul auch mehrere Veranstaltungen geben, von denen man sich eine aussuchen kann. Fürs erste Semester ist das aber einfach und es gibt zu jedem Modul eine Veranstaltung (und umgekehrt).





