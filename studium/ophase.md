---
title: "O-Phase 2022"
slug: "ophase"
lang: de
relevant:
  end: 2022-10-21
  prio: 5
#link: https://fsr.math-cs.uni-goettingen.de/de/o-phase/
link: https://fsr.math-cs.uni-goettingen.de/de/o-phase/informatik-o-phase/
tags: event

---


Du wirst eine tolle O-Phase haben.
Die wichtigsten Informationen gibt es [hier](https://fsr.math-cs.uni-goettingen.de/de/o-phase/) auf der Website des Fachschaftsrates Mathematik, Informatik und Data Science.

![Erstiheft 2022 Deckblatt](file:Erstiheft_Deckblatt_2022_Informatik.png)

