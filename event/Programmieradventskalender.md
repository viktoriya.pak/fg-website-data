---
title: "Programmier&shy;adventskalender"
slug: aoc
lang: de
relevant:
  end: 2022-12-31
  prio: 3
tags: event
---

:::info
**tl;dr:**
Mach mit bei unserem Programmieradventskalender!
- https://aoc.fg.informatik.uni-goettingen.de/
- Zeitraum: Advent.
- Es gibt Preise zu gewinnen.
:::

![Plakat 2022](file:AOC9.png)

Die Fachgruppe Informatik betreibt dieses Jahr zusammen mit der Fachgruppe DataScience, der Fachgruppe Mathematik und der Fachschaft Physik einen Programmieradventskalender mit den Rätseln vom AdventOfCode. Mach mit und löse die Rätsel. Du kannst dich dabei mit deinen Kommiliton:innen messen oder einfach deine Programmierskills verbessern anhand von praktischen Aufgaben.

Der AdventOfCode ist ein Event, welches seit 2015 jährlich stattfindet. Dabei wird jeden Tag ein kleiens Puzzle veröffentlicht, das man versuchen kann zu lösen. Was das mit Programmieren zu tun hat? Naja das Puzzle besteht zumeist aus tausenden Zeilen und es ist besser wenn du ein Programm schreibst, dass die Aufgabe für dich löst.

Teilnehmen kann am AdventOfCode jeder. Die Programmierrätsel zielen auf verschiedene Fähigkeiten und Niveaus ab, die in jeder beliebigen Programmiersprache gelöst werden können. Menschen aus der ganzen Welt nutzen sie als Schnelligkeitswettbewerb, zur Vorbereitung auf Vorstellungsgespräche, für Firmenschulungen, als Universitätskurse, einfach als Übung oder um sich gegenseitig herauszufordern.

Wir erstellen eine seperate Seite um die gelösten Rätsel unserer Studierenden zu tracken. Und noch eine kleine Motivation: Du kannst auch was gewinnen. Je mehr Aufgaben du löst, desto wahrscheinlicher ist es :)

:::success
Unser Leaderboard wird auf folgender Seite geführt: https://aoc.fg.informatik.uni-goettingen.de/
:::


Du benötigst **keinen** Account auf der [Seite vom AdventOfCode](https://adventofcode.com/).

