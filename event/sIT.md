---
title: "studentische Informatiktage (sIT)"
slug: sit
lang: de
---

Die studentischen Informatiktage sind ein Vortrags- und Workshopwochenende, die jedes Jahr im Sommersemester von der Fachgruppe Informatik veranstaltet werden. Das Programm ist immer sehr divers und es ist für jede\*n was dabei.

Von Freitag bis Sonntag gibt es ein buntes Programm aus:
- Vorträge und Workshops von Firmen
- Vorträge und Workshops von Alumni und Studierenden
- ein Soziales Rahmenprogramm (Grillen, Jeopardy, etc.)

**[zur sIT-Website (2022)](https://sit.fg.informatik.uni-goettingen.de/)**

