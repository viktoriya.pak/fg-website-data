---
title: "Flohmarkt"
slug: flohmarkt
lang: de
relevant:
  end: 2022-11-06
  prio: 4
tags: event
---

Du bist neu im Studium und brauchst noch einen Kochlöffel für deine Wohnung? Oder bist du schon länger hier, und hast zu Hause aufgeräumt, und möchtest eine Tastatur oder ein paar Schreibblöcke los werden?

![Flohmarkt-Illustration](file:flohmarktoh.png)

Am 6. November gibt es einen Flohmarkt im [Institut für Informatik](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110), insbesondere für Küchen- und Studienbedarf.

- Ab 14 Uhr Aufbau der Stände
- 15 bis 18 Uhr Flohmarkt

Wenn du etwas verkaufen oder verschenken möchtest, melde dich bitte vorher per Mail bei uns: fachgruppe@informatik.uni-goettingen.de.
Für größere Gegenstände, die du nicht her transportieren willst, wird es ein schwarzes Brett geben, an dem du auch Angebote für Schreibtische oder Ähnliches aushängen kannst. Für den Transport sprichst du dich dann direkt mit interessierten Leuten ab.

Wenn du nur etwas zu Verschenken hast, kannst du auch einfach einen Karton am Vormittag vorbeibringen und ab 17 Uhr wieder abholen, anstatt ihn die ganze Zeit zu betreuen. Auch dafür melde dich bitte vorab per Mail bei uns.
