---
title: "Weihnachts&shy;vorlesung"
slug: weihnachtsvorlesung
lang: de
---
Die Weihnachtsvorlesung findet einmal im Jahr kurz vor Weihnachten statt. Mit Keksen und weihnachtlichen Getränken sitzen wir gemütlich zusammen und hören uns Vorträge von Leuten an, die sonst Vorlesungen in der Informatik oder Data Science halten. Gerne gesehen sind auch Mitarbeitende, die sonst von den meisten Studierenden nicht wahrgenommen werden. Die einzige Voraussetzung ist, dass die Vorträge unterhaltsam sind. Das ist uns bisher jedes Jahr gut gelungen.
Dieses Jahr am 15.12.2022 um 19:00 Uhr in MN09 findet die Veranstaltung endlich wieder wie gewohnt statt. Es wird spannende Vorträge von Professor Florin Manea und Engelbert Suchla geben.

![Weihnachtsvorlesungsplakat 2022](file:weihnachtsvorlesung_2022.jpg)
