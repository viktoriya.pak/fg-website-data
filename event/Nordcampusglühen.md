---
title: "Nordcampus-Glühen"
slug: nordcampusgluehen
lang: de
relevant:
  end: 2022-11-29
  prio: 5
tags: event
---

Das Nordcampus-Glühen findet am Fr. 25.11. ab 16 Uhr [an den Containergebäuden an der Bushaltestelle Tammannstraße](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.101) statt. Als Gemeinschaftsprojekt der Nordcampusfachschaften/-Gruppen wird es an ein paar Ständen Glühwein, Punsch und vieles mehr geben. Alle und vor allem Nordcampus-Studis sind herzlich eingeladen, vorbeizuschauen.

:::warning
Bringt bitte eine eigene Tasse für Heißgetränke mit!
:::

![Freut euch auf heiße Getränke!](file:nordcampusglühen.png)
