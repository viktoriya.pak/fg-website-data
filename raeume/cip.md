---
title: "Rechnerräume"
slug: cip
lang: de
---

Die CIP-Pools sind über mehrere Gebäude verteilt, [drei davon in der Etage -1 im IfI](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) und [einer im Provisorium](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.103). In den Räumen befinden sich Computer zur freien Verfügung. Hier treffen sich auch Lerngruppen und Studierende tauschen Informationen zu Modulen oder dem Studium allgemein aus. Studierende aus höheren Semstern können hier angetroffen werden, die meist auch gerne helfen wenn man Probleme hat.

![CIP-Pool -1.101](file:cip1101_leer.JPG)

[TOC]

## Die Computer

An den Computern kann man sich mit seinem Studiaccount anmelden. Beim ersten anmelden wird ein Account erstellt. Das kann einige Minuten dauern. Jeder Account bekommt 10GB Speicher zugewiesen. Die Computer laufen auf Ubuntu (Linux) und haben alles, was für die Vorlesungen benötigt wird, installiert. Bugs und Anregungen können unter https://gitlab.gwdg.de/cip/pool-bugs/-/issues gemeldet werden. 

## Follow-Me Drucker
Im Raum -1.110 befindet sich einer der Follow-Me Drucker, wie es sie an der ganzen Universität verteilt gibt. Druckaufträge, die ihr im Webportal (https://print.student.uni-goettingen.de) hochgeladen habt, könnt ihr dort mithilfe eures Studienausweises drucken.
Die Besonderheit im IfI ist, dass ihr von den Computern in den CIP-Pools aber auch direkt auf den Follow-Me Drucker im CIP-Pool zugreifen könnt.

## Weitere Ausstattung

Alle CIP-Pool Räume sind ausgestattet mit mindestens einen Beamer (primär für Veranstalltungen) und einem Whiteboard oder SmartBoard. Diese können ebenfalls von Studierenden genutzt werden, solange niemand durch diese Nutzung gestört wird.

- Im [Raum -1.110](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110) ist im ersten Schrank links vom Eingang ein Tacker und ein Bindeset (für das Binden von Büchern, Skripten, Papern, usw.).
- Im [Raum -1.111](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) ist ein Regal mit verschieden Büchern. 
- [Raum -1.101](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.101) und [der CIP-Pool im Provisorium](https://lageplan.uni-goettingen.de/?ident=1487_1_EG_0.103) sind auch Seminarräume und sind dementsprechend ausgestattet.

Die Räume [-1.110](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.110), [-1.111](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.111) und [-1.101](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.101) sind je mit einer Klimeanlage und einem Lufttauscher ausgestattet. Letzterer tauscht automatisch Innenluft mit frischer Außenluft und bringt diese auf Raumtemperatur. Vor allem im Winter und Hochsommer ist langes manuelles Lüften daher nicht sinnvoll.

![CIP-Pool 1.110](file:cip1110_leer.JPG)
