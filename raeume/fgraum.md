---
title: "Fachgruppenraum"
slug: fgraum
lang: de
tags: fachgruppe
---

Der Fachgruppenraum ist explizit als Aufenthaltsraum zum Sozialisieren und Studieren für alle Studis der Fachgruppe Informatik (also du!) gedacht.

Im Fachgruppenraum steht ein Sofa, Stühle und Tische, an denen du chillen oder dich mit deinen Kommoliton:innen :tipping_hand_woman: austauschen kannst.
Der Fachgruppenraum befindet sich in der [Etage -1 im Raum -1.108 des Institutes für Informatik](https://lageplan.uni-goettingen.de/?ident=2412_1_1.UG_-1.108).

![Der Fachgruppenraum](file:fgraum_2020.jpg "Der Fachgruppenraum im Jahre 2020.")

[TOC]

## :money_with_wings: Kasse des Vertrauens (KDV) {#kdv}

Im Fachgruppenraum befindet sich auch die KDV (Kasse des Vertrauens). Das ist *alles im und um* dem Getränkekühlschrank.

Hier kannst du dir die zur Verfügung stehenden **Getränke** (meist Cola, Wasser, Mate und Spezi) :champagne: nehmen und etwas Geld (z.B. 1€) in die Kasse oben auf dem Kühlschrank legen.
In der KDV gibt es auch **Kekse**! :cookie:

:::warning
Bitte die leeren Flaschen wieder an die KDV zurückgeben! :recycle: Die KDV ist auf das Pfand angewiesen.
:::

Während besonderen Fachgruppenveranstaltungen (wie z.B. [gIT](slug:sit) oder [O-Phase](slug:ophase)) ist die KDV manchmal leider für einige Tage unzugänglich.

:::success
Die KDV ist vollständig selbstorganisiert und bleibt -- so wie vieles in der Fachgruppe -- durch freiwilliges Engagement von Studierenden erhalten, welche z.B. mit dem Bollerwagen regelmäßig das Pfand wegbringen und neue Getränke einkaufen. Das Geld dafür gibt die KDV mit freuden (einfach Jake kontaktieren).
**Also hilf doch auch mal beim nächsten Getränkeeinkauf mit! :shopping_cart:**
:::

## :game_die: Spieleschrank {#spieleschrank}

Im Spieleschrank befinden sich viele verschiedene Brett- und Kartenspiele :jigsaw:, welche bei [den wöchentlichen Spieleabenden](slug:spieleabend) verwendet werden.

Der Schrank ist aber normalerweise abgeschlossen :lock:. Fragt ruhig jemanden aus einem höheren Semester im [CIP-Pool](slug:cip) falls ihr an den Schrank wollt, viele Personen können euch da weiterhelfen :unlock:.


## :joystick: Retro Gaming Station {#retrogamingstation}

Während es im [Spieleschrank](#spieleschrank) nur analoge Spiele gibt, gibt es auf dem Tisch im Fachgruppenraum auch digitale Spiele in Form der *Retro Gaming Station*.
Hier kannst **du mit bis zu drei weiteren Personen** gute alte Spiele spielen.

Die Retro Gaming Station besteht aus einer Raspberry Pi Tastatur :keyboard:, einem Bildschirm :desktop_computer: und vier Controllern :video_game:.

Auf der Retro Gaming Station sind verschiedene Emulatoren und Spiele installiert (Gamecube, N64, SNES, etc...).

Nutze die Retro Gaming Station um neue Freunde in der Studierendenschaft zu finden, eine Pause zwischen Vorlesungen zu machen, dem Prüfungsstress entgegen zu wirken, oder das Lernen für deine Klausur nächste Woche zu umgehen :wink:.

