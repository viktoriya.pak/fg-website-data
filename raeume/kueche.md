---
title: "Küche"
slug: kueche
lang: de
---

Die Kaffeeküche befindet sich in [Etage -1 im Institut für Informatik im rechtsseitig gelegenen Flur](https://www.geodata.uni-goettingen.de/lageplan/?ident=2412_1_1.UG_-1.113). Die Küche ist für alle gedacht. Damit das klappt und wir uns mit der Benutzung alle wohlfühlen, braucht es ein paar gemeinsame Regeln, die auch auf den Schildern in der Küche zu finden sind.
Die Fachgruppe Informatik kümmert sich darum, dass die Küche mit den wichtigsten Verbrauchsgegenständen ausgestattet ist. Wenn davon etwas leer ist, sagt uns einfach Bescheid: fachgruppe@informatik.uni-goettingen.de oder es ist gerade wer im Fachgruppenraum.

![Ein Foto der Küche in aufgeräumten Zustand](file:kueche2022.jpg "Die Küche")

[TOC]

## Verbrauchsgüter
Die Fachgruppe Informatik kümmert sich um Nachschub
* Spülmaschinentabs
* Spülmittel
* Spüllappen
* Edding

## Benutzung der Küche
:::success
Wer die Küche benutzt sollte auch dabei helfen diese sauber zu halten.
:::

### Der Geschirrspüler

_Habt ihr Geschirr benutzt?_ Tut es einfach in den Geschirrspüler und lasst es nicht im Fachgruppenraum oder auf der Spüle liegen.

_Ist der Geschirrspüler voll und sauber?_ Kein Problem, räumt ihn doch bitte aus, ihr habt ja auch schonmal etwas reingeräumt. 
Wir als Studierende der Informatik sind alle zusammen für das Instandhalten der Küche verantwortlich.

_Ist der Geschirrspüler voll und dreckig?_ Auch kein Problem, die Spültabs sind in der Schublade über dem Kühlschrank, das voreingestellte Standardprogramm funktioniert wunderbar. Zum Anstellen den Power-Knopf betätigen und dann die Tür zudrücken bis sie einrastet und das Spülprogramm beginnt. 

### Der Kühlschrank

Der Kühlschrank kann für Essen und Getränke von allen Studierenden genutzt werden. 

Hierbei ist es allerdings wichtig, dass ihr __eure Sachen beschriftet__ (Name/Pseudonym und Öffnungsdatum wenn ihr es länger dort lagert). Alles unbeschriftete gilt als Allgemeingut und darf frei verwendet werden. 

Einen __Permanentmarker__ zum Beschriften findet ihr in der Schublade über dem Kühlschrank. 

### Mikrowelle

Es gibt zwei __Mikrowellenhauben__. Bitte benutzt diese (oder eine andere Form von Abdeckung) wenn ihr euch etwas in der Mikrowelle aufwärmt. Ist eine Mikrowellenhaube dreckig gehört sie mit in die Spülmaschine oder kurz per Hand abgewaschen.
Findet ihr eine dreckige Mikrowelle vor, hat sich leider wer anderes nicht daran gehalten. Auch wenn ihr das nicht verursacht habt, wäre es dann dennoch schön, wenn ihr die Mikrowelle dann sauber macht.

### Wasserboiler

:::warning
Bitte zurzeit nicht benutzen. Wasserboiler können viel Volumen zur Zeit erhitzen, benötigen jedoch verhältnismäßig viel Energie.
:::

Das Gerät über dem Spülbecken ist ein Wasserboiler mit integriertem Wasserhahn. Hier könnt ihr sowohl kaltes Leitungswasser als auch gekochtes Wasser (sofern welches gekocht wurde) zapfen. 
Es hängt auch __eine bebilderte Anleitung obendrüber__, die euch erklärt wie das ganze funktioniert. 

### Wasserkocher

Wenn euch der Wasserboiler zu sehr einschüchtert oder es schnell gehen soll, gibt es auch einen herkömmlichen Wasserkocher auf der Anrichte, den ihr benutzen könnt. Beachtet bitte das ihr den nur bis zur "max" Markierung füllt um Überflutungen zu vermeiden. 

### Kaffeemaschine

Die Kaffeemaschine darf gerne genutzt werden um Kaffee zu kochen. 
Meist lohnt es sich rumzufragen ob noch andere Kaffee trinken möchten und dann eine große Kanne zu kochen. Die Kaffeemaschine kann den Kaffee auch warm halten wenn sie nicht ausgeschaltet wird. 

Bitte reinigt den Filterhalter und die Kanne bevor ihr geht. 

Wie man Kaffee in der Filtermaschine kocht: 

* Wenn an, dann die Kaffeemaschine ausschalten um die Heizelemente abzuschalten.

* Einen Kaffeefilter an den perforierten Seiten umfalten, auffalten und in den Filterhalter stecken

* Die gewünschte Menge an Wasser + 1 Tasse extra (geht beim Brühvorgang verloren) in den Wassertank füllen. Dafür gern einfach die Kanne und Messskala am Tank nutzen. 

* Das Kaffeepulver in den Filter füllen. Grob kann man 1 Löffel Pulver pro Tasse Wasser rechnen. 

* Den Filterhalter in Position über der Kanne bringen, die Kaffeemaschine rechts am roten Kippschalter anschalten und warten bis das Wasser durchgelaufen ist. Die Maschine macht dann schnaufende Geräusche wenn sie fertig ist. 


### Tee, Kaffee, etc.

Sowohl Kaffee als auch Tee in den Schränken sind für die Allgemeinheit gedacht. 
Hier dürfen sich alle bedienen. 

Wichtig hierbei ist, dass die Vorräte von Studierenden für Studierende sind.

Das bedeutet: Wenn ihr diese benutzt, stellt auch irgendwann selbst mal wieder etwas dazu, damit dieses System weiterhin funktioniert.

Ausnahmen sind natürlich beschriftete Dinge, achtet allerdings bitte darauf die Schränke nicht mit Privateigentum zu überfüllen. 

---

## FAQ
- __Ich möchte dabei unterstützen, dass die Küche in sauberem und ordentlichem Zustand ist, was kann ich tun?__ : Die eigenen Sachen im Kühlschrank nicht schlecht werden lassen, saubere Spülmaschinen ausräumen, große Sachen per Hand abwaschen, Mikrowelle sauber machen sind die besten Hilfen.
- __Hilfe, etwas von meinem Essen ist aus dem kühlschrank verschwunden.__ : Ab und an erbarmt sich eine Person den Kühlschrank sauber zu machen und auszusortieren, was nicht beschfriftet, schlecht und/oder abgelaufen ist. Wahrscheinlich ist das passiert und deine Sachen haben auf diese Kriterien zugetroffen.
- __In der Küche hängt ein Schild, dass ich meine Sachen aus dem Kühlschrank räumen soll. Wieso?__ : Wahrscheinlich ist bald O-Phase oder eine andere Veranstaltung bei der gekocht wird. Die Einkäufe dafür brauchen ziemlich viel Platz, weshalb FSR und Fachgruppe auf den Platz angewiesen sind.
- __Sache X fehlt mir in der Küche__ : Dann schreib deinen Vorschlag gerne an die Fachgruppe. Wir schauen dann, ob sich deine Idee umsetzen lässt.
- __Ich habe eine Idee für dieses mini-FAQ__ : Dann schreib das gerne der Fachgruppe. 


