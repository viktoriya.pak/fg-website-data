---
title: "Fachgruppen&shy;treffen"
slug: fgtreffen
tags: engage, event, fachgruppe
lang: de
relevant: 0
---

Auf Fachgruppentreffen besprechen wir in geselliger Runde die aktuell anstehenden Themen, planen Veranstaltungen und unterhalten uns übers Studium und Studienqualität. Wir freuen uns immer über Interessierte, also komm vorbei, auch wenn du "nur mal reinschnuppern" oder nette Leute kennenlernen willst!

Momentan treffen wir uns Dienstags um 18:00 im [IfI Raum 0.101](https://www.geodata.uni-goettingen.de/lageplan/?ident=2412_1_EG_0.101). Du kannst aber auch online dabei sein. Den Link zum BBB Raum schicken wir immer mal wieder per E-Mail rum, du bekommst ihn aber natürlich auch, wenn du uns danach fragst. Das geht ganz einfach, klick einfach **[hier](mailto:fachgruppe@informatik.uni-goettingen.de?subject=Fachgruppentreffen&body=Hi%2C%0D%0A%0D%0Aich%20w%C3%BCrde%20gerne%20mal%20zu%20einem%20Fachgruppentreffen%20vorbei%20kommen%2C%20k%C3%B6nnt%20ihr%20mir%20den%20Link%20daf%C3%BCr%20schicken%3F%0D%0A%0D%0AViele%20Gr%C3%BC%C3%9Fe%2C%0D%0A%3CName%3E)**.

Einmal im Semester gibt es außerdem eine Fachgruppenvollversammlung, da berichten wir, was sich so getan hat und sprechen typischerweise über allgemeine Themen des Studiums bei denen alle gut mitreden können.

