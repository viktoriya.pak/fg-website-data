---
title: "Mentoring"
slug: mentoring
status: draft
lang: de
---

Mit Beginn des Wintersemesters 2021/22 wird die Fachgruppe Informatik das erste Mal ein Mentoring-Programm anbieten.  
Wir wollen euch, den Erstsemestern der angewandten Informatik, ermöglich, einen besseren Einstieg in die Informatik zu bekommen sowie einfacher Kontakte zu knüpfen und zu halten.  
Die Teilnahme ist freiwillig.

## Ablauf
Nach dem Ende der [O-Phase](slug:ophase) werden wir Menteegruppen bilden, die sich aus einigen wenigen Erstsemestern (Mentees) und 1-2 Mentor:innen zusammensetzen. Mentor:innen sind dabei andere Studierende der angewandten Informatik (Semster 3 und höher).  
Diese Gruppen werden über eure ersten beiden Semester bestehen und ihr erhaltet die Möglichkeit, euch alle zwei Wochen zu treffen, um euch gemütlich zusammenzusetzen und auszutauschen.

## Matching
Matching, das Zuordnen von Mentor:innen und Mentees, ist ein zentraler Baustein am Anfang des Programmes, mit dem wir erreichen wollen, dass ihr Gruppen findet, in denen ihr euch wohlfühlt. Wie das im Detail aussehen wird, werden wir euch in den kommenden Wochen mitteilen.

## Veranstaltungen im Rahmen des Mentorings
Details zu den Veranstaltung folgen zu späterem Zeitpunkt.

## Ich will mitmachen
Du willst an dem Programm als Mentor:in oder Mentee teilnehmen? Dann schreib uns direkt eine Email an [fachgruppe@informatik.uni-goettingen.de](mailto:fachgruppe@informatik.uni-goettingen.de)
