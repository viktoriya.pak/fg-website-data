--- 
title: "Fußzeile"
status: hidden
---

:::::::::::::: {.columns}
::: {.column style="--col-width: 20%;"}

[![Logo](file:banner-logo.png "Startseite"){style="background-color:#fff;border-radius:0.5em;padding:0.2em;"}](slug:index)

:::
::: {.column style="--col-width: 30%;"}
:::
::: {.column style="--col-width: 50%;"}

[:arrow_up: Zurück nach oben.](#top)

:::
::::::::::::::

:::::::::::::: {.columns}
::: {.column style="--col-width: 50%;"}

## Rechtliches

- [Datenschutz](slug:datenschutz)
- [Impressum](slug:impressum)

## Kontakt

| Fachgruppe Informatik Georg-August-Universität Göttingen
| Goldschmidtstr. 7
| 37077 Göttingen 

:::
::: {.column style="--col-width: 50%;"}

## Links

- [Über uns](slug:fachgruppe)
- [Lageplan](slug:lageplan)
- [Webseite des Fachschafts&shy;rates](https://fsr.math-cs.uni-goettingen.de/de/)
- [AStA Uni Göttingen](https://asta.uni-goettingen.de/)
- [Institut für Informatik](https://www.informatik.uni-goettingen.de)
- [Studiwerk](https://www.studentenwerk-goettingen.de/)
- [Universität Göttingen](https://www.uni-goettingen.de/)
<!--- [Fachgruppe DataScience](tba)-->

:::
::::::::::::::

:email:&nbsp;fachgruppe{at}informatik.uni-goettingen.de

© Fachgruppe Informatik Uni Göttingen, 2022.

